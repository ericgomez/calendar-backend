const mongoose = require('mongoose');

const dbConnection = async () => {
  try {
    await mongoose.connect(process.env.DB_CONNECTION);
    console.log('MongoDB connected');
  } catch (error) {
    console.log(error);
    throw new Error('MongoDB connection error');
  }
};

module.exports = { dbConnection };
