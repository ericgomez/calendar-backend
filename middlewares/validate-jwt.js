const { response } = require('express');
const jwt = require('jsonwebtoken');

const validateJWT = (req, res = response, next) => {
  // x-token header
  const token = req.header('x-token');

  if (!token) {
    return res.status(401).json({
      ok: false,
      error: 'No token provided'
    });
  }

  // valid token - synchronous
  try {
    const { uid, name } = jwt.verify(token, process.env.JWT_SECRET);

    req.uid = uid;
    req.name = name;
  } catch (error) {
    console.log(error);
    res.status(400).json({
      ok: false,
      error: 'Invalid token'
    });
  }

  next();
};

module.exports = { validateJWT };
