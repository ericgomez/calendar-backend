require('dotenv').config()

const config = {
  // when you want to use a different port for the server
  dev: process.env.NODE_ENV !== 'production',
  port: process.env.PORT || 3000
}

module.exports = { config }
