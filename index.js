const express = require('express');
const cors = require('cors');
const { config } = require('./config/index');
const { dbConnection } = require('./database/config');

// Create a new express application instance
const app = express();

// database
dbConnection();

// *** Middleware ***

// CORS
app.use(cors());

// Directory for static files
app.use(express.static('public'));

// lecture and parse request body
app.use(express.json());

// Routes
app.use('/api/auth', require('./routes/auth'));

// listen for requests :)
const listener = app.listen(config.port, () => {
  console.log('Your app is listening on port ' + listener.address().port);
});
