/*
  Routes for authentication
  host + /api/auth
*/

const { Router } = require('express');
const { check } = require('express-validator');

const { createUser, loginUser, renewToken } = require('../controllers/auth');
const { validateField } = require('../middlewares/validate-field');
const { validateJWT } = require('../middlewares/validate-jwt');

const router = Router();

/*
 ***** Routes *****
 */

router.post(
  '/new',
  [
    check('name', 'Name is required')
      .not()
      .isEmpty(),
    check('email', 'Email is required').isEmail(),
    check(
      'password',
      'Password is required and must be at least 6 characters long'
    ).isLength({
      min: 6
    }),
    validateField // custom middleware
  ],
  createUser
);

router.post(
  '/',
  [
    check('email', 'Email is required').isEmail(),
    check('password', 'Password is required').exists(),
    validateField // custom middleware
  ],
  loginUser
);

router.get('/renew', validateJWT, renewToken);

module.exports = router;
